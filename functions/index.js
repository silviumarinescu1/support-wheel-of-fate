const functions = require("firebase-functions");

const admin = require("firebase-admin");
const app = admin.initializeApp();
const db = app.firestore();

const process = async (event) => {
  switch (event.type) {
    case "engineerCreate":
      await db.collection(`engineers`).add(event.payload);
      break;
    case "engineerRemove":
      await db.doc(`engineers/${event.payload.id}`).delete();
      break;
    case "engineerUpdate":
      await db.doc(`engineers/${event.payload.id}`).set(event.payload);
      break;
    case "addBau":
      await db.collection(`bau`).add(event.payload);
      break;
    case "setBau":
      await db.doc(`bau/${event.payload.id}`).set(event.payload);
      break;
  }
};

exports.eventTriguer = functions.firestore
  .document("events/{eventId}")
  .onCreate((snap, context) =>
    process({ ...snap.data(), id: context.params.eventId })
  );
