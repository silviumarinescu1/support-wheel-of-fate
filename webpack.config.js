const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const { basename } = require("path");

const mode = process.env.NODE_ENV || "development";
const prod = mode === "production";

module.exports = {
  entry: {
    bundle: ["./src/index.js"]
  },
  devServer: {
    historyApiFallback: true
  },
  resolve: {
    alias: {
      svelte: path.resolve("node_modules", "svelte")
    },
    extensions: [".mjs", ".js", ".svelte"],
    mainFields: ["svelte", "browser", "module", "main"]
  },
  output: {
    path: __dirname + "/build",
    filename: "[name].js",
    chunkFilename: "[name].[id].js"
  },
  module: {
    rules: [
      {
        test: /\.(gif|png|jpe?g)$/i,
        use: [
          "file-loader",
          {
            loader: "image-webpack-loader",
            options: {
              bypassOnDebug: true,
              disable: true
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader"
      },
      {
        test: /\.svelte$/,
        use: {
          loader: "svelte-loader",
          options: {
            emitCss: true,
            hotReload: true,
            preprocess: require("svelte-preprocess")({
              /* options */
            })
          }
        }
      },
      {
        test: /\.css$/,
        use: [prod ? MiniCssExtractPlugin.loader : "style-loader", "css-loader"]
      }
    ]
  },
  mode,
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css"
    }),
    new HtmlWebpackPlugin({
      title: "Support Wheel of Fate",
      meta: {viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'}
    }),
    new FaviconsWebpackPlugin({
      logo: "./src/images/favicon.svg",
      mode: "webapp",
      devMode: "webapp",
      favicons: {
        appName: "Support Wheel of Fate",
        appDescription: "Support Wheel of Fate",
        developerName: "silviu",
        developerURL: null,
        background: "#d8d8d8",
        theme_color: "#08bfb0",
        icons: {
          coast: false,
          yandex: false
        }
      },
      inject: htmlPlugin =>
        basename(htmlPlugin.options.filename) === "index.html"
    })
  ],
  devtool: prod ? false : "source-map"
};
