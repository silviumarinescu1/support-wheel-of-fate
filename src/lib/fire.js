import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

let firebaseConfig = {
  apiKey: "AIzaSyAgHKsMsTRxzkTqfBo_rmL3QVehxOlIqY4",
  authDomain: "support-wheel-of-fate-aabe0.firebaseapp.com",
  databaseURL: "https://support-wheel-of-fate-aabe0.firebaseio.com",
  projectId: "support-wheel-of-fate-aabe0",
  storageBucket: "support-wheel-of-fate-aabe0.appspot.com",
  messagingSenderId: "41937576355",
  appId: "1:41937576355:web:178af36fb5039bae5dba16",
  measurementId: "G-8J3TQ3FHGE",
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const storage = firebase.storage();
export { db, storage };
